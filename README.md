The WebSensor Project
=====================

A huge barrier in Web Application Security Research is the absence of data that
can be studied. Usually application data is far to sensitive to be shared among
researchers. Honeypots are an interesting concept for collecting data that is not
(or shall not) be related to any *real* application.

The *WebSensor* project aims at providing a web honeypot that collects data in
the format of the ModSecurity audit log. This allows the use of tools like the
*jwall-tools* to replay the recorded HTTP traffic and extract statistics that
are valuable for other OWASP projects, like the OWASP ModSecurity Core Rules
project.


### Community Oriented

The main idea of the *WebSensor* project to spawn a community for data collection
and data sharing. That is, each participant can run one or more sensors and the
data will be collected in a central system. The collected data is then accessible
to any participant of the project.


### Early Stage

The project is currently in a very early stage. A central machine is being set up
for storing the logs provided by sensors using the jwall.org [AuditConsole](http://www.jwall.org/AuditConsole).
In addition a simple Java based honeypot server is under development and will soon
be released to allow for gathering data.

If you are interested in the project, then you're welcome to subscribe our *WebSensor*
[mailing list](https://secure.jwall.org/mailman/listinfo/websensor).
