
Feeding Back - a Pool of Data
===========================================

The data collected by the *WebSensors* project will be fed into a large pool. This pool shall serve
as playground for application security research. Testing methodologies, raising statistics and giving
researchers the possibility to compare their methods on a shared data set.


Feeding the ModSecurity Core Rules
----------------------------------
To a large degree, the collected data shall be fed back into other projects, such as the [ModSecurity
Core Rules](https://www.owasp.org/index.php/Category:OWASP_ModSecurity_Core_Rule_Set_Project). By
recording the complete HTTP traffic that hits the *WebSensors* instances, we are able to replay the
HTTP traffic and run it through different versions of the CRS or any other web intrusion detection
system.

The statistics (e.g. rules that fired most, countries attacks came from) will be valuable to improve
the rules for the WAFs/Web IDSes and support the community spirit that we would like to establish
with the jwall.org *WebSensors* project.

