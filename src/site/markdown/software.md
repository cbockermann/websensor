Software used &amp; produced
============================

A large part of the *WebSensor* project is about developing software that
makes up a honeypot and allows for sending audit data to the central collector.
The collector is powered by the [AuditConsole](http://www.jwall.org/AuditConsole)
project, which provides a free web based server for collecting audit data. In
addition, a list of open-source libraries gives power to our honeypot server:

   1. the *streams* Framework
   2. the *web-audit* library
   3. the *jwall-tools* toolbox.

By building upon the free software provided at [jwall.org](http://www.jwall.org/),
the heart of the *WebSensor* project - the honeypot server - is able to re-use a
large portion of existing libraries, such as the [streams](http://www.jwall.org/streams/)
framework for log processing, the [web-audit](http://www.jwall.org/web/audit/index.jsp)
library for handling ModSecurity audit-logs and the [jwall-tools](http://www.jwall.org/jwall-tools),
which allow for uploading audit log data to a remote AuditConsole server.