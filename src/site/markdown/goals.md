Goals &amp; Use Cases
=====================

Running a honeypot may be of interest to gather a variety of different events. Most importantly
it can be used to capture automated attacks and attack probes, i.e. attackers automatically
searching for targets. Even though this does not in particular help to protect a specific web
page against intrusions, honeypot data may indicate *shifts* in the behavior of attackers, that
may suggest new, unknown types of attacks or methods.

The goal of the *WebSensors* project is to plant a large collection of sensors in different spots
of the internet. Each sensor runs an HTTP server that serves no real application. The purpose of
this server is to accept connections and simply record the web traffic. In general the server
will simply respond with status *404 - not found*.


Getting on - as easy as possible
--------------------------------

The predominant goal of the *WebSensors* project is to provide the software and means to get a
network of sensors running as easy as possible. We would like to help people being able to contribute
to the project by setting up their own sensor within minutes.

One option to participate in the project will be to use the Amazon Micro Instances, which are
virtual servers that are provided for free for 12 month to new registrants of the Amazon cloud.
Building packages for those kind of machines is one of our first main goals, which will enable
people to run a sensor in different data centers over the world within just minutes of setup
time.

