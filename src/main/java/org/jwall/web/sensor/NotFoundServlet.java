package org.jwall.web.sensor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NotFoundServlet extends HttpServlet {

	/** The unique class ID */
	private static final long serialVersionUID = -4822419400625713032L;

	static Logger log = LoggerFactory.getLogger(NotFoundServlet.class);

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		log.info("Serving {}-request {}", req.getMethod(), req.getRequestURI());
		sendNotFound(resp);
		return;
	}

	protected void sendNotFound(HttpServletResponse resp)
			throws ServletException, IOException {
		resp.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}