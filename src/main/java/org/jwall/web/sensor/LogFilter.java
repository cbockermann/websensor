package org.jwall.web.sensor;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogFilter implements Filter {
	static Logger log = LoggerFactory.getLogger(LogFilter.class);

	public boolean doLog(String method) {
		return true;
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		if (servletRequest instanceof HttpServletRequest) {

			final SimpleDateFormat fmt = new SimpleDateFormat(
					"'['dd/MMM/yyyy:hh:mm:ss' 'Z']'");

			final HttpServletRequest request = (HttpServletRequest) servletRequest;
			// final HttpServletResponse response = (HttpServletResponse)
			// servletResponse;

			String txId = (String) request.getAttribute("txId");
			if (txId == null) {
				txId = UUID.randomUUID().toString().toUpperCase();
				request.setAttribute("txId", txId);
			}

			log.trace("Request headers:");
			String uriRaw = request.getRequestURI();
			if (request.getQueryString() != null)
				uriRaw = request.getRequestURI() + "?"
						+ request.getQueryString();

			log.trace("  {} {} " + request.getProtocol(), request.getMethod(),
					uriRaw);
			// Enumeration<?> en = request.getHeaderNames();
			// while (en.hasMoreElements()) {
			// String key = en.nextElement().toString();
			// log.trace("  {} = {}", key, request.getHeader(key));
			// }

			// Check wether the current request needs to be able to support the
			// body to be read multiple times
			if (doLog(request.getMethod())) {
				// Override current HttpServletRequest with custom
				// implementation
				MultiReadHttpServletRequest req = new MultiReadHttpServletRequest(
						request);

				if (request.getContentLength() > 0) {
					log.trace("RequestBody:");
					BufferedReader reader = req.getReader();
					String line = reader.readLine();
					while (line != null) {
						log.trace("  {}", line);
						line = reader.readLine();
					}
					reader.close();
				}
				filterChain.doFilter(req, servletResponse);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream evt = new PrintStream(baos);
				addSeparator(evt, txId, 'A');
				evt.print(fmt.format(new Date()));
				evt.print(" " + txId);
				evt.print(" ");
				evt.print(request.getRemoteAddr());
				evt.print(" ");
				evt.print(request.getRemotePort());
				evt.print(" ");
				evt.print(request.getLocalAddr());
				evt.print(" ");
				evt.println(request.getLocalPort());

				addSeparator(evt, txId, 'B');
				Enumeration<?> en = request.getHeaderNames();
				while (en.hasMoreElements()) {
					String key = en.nextElement().toString();
					evt.print(key);
					evt.print(": ");
					evt.println(request.getHeader(key));
				}
				evt.println();

				if (request.getContentLength() > 0) {
					addSeparator(evt, txId, 'C');
					byte[] buf = new byte[4096];
					InputStream in = req.getInputStream();
					int read = in.read(buf);
					while (read > 0) {
						evt.write(buf, 0, read);
						read = in.read(buf);
					}
					evt.println();
				}

				addSeparator(evt, txId, 'F');
				evt.println("RESPONSE_HEADER Logging not yet implemented.");
				evt.println();

				addSeparator(evt, txId, 'Y');
				evt.println("Log created by jwall.org WebSensor version 0.0.1");
				addSeparator(evt, txId, 'Z');

				evt.flush();
				evt.close();

				log.info("Event:\n{}", new String(baos.toByteArray()));
				return;
			}
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	public static void addSeparator(PrintStream writer, String txId,
			char section) {
		writer.print("--");
		writer.print(txId);
		writer.print("-");
		writer.print(section);
		writer.println("--");
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}